/**
 * Data Access Objects used by WebSocket services.
 */
package com.ecas.web.websocket.dto;
