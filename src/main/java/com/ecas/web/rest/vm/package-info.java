/**
 * View Models used by Spring MVC REST controllers.
 */
package com.ecas.web.rest.vm;
